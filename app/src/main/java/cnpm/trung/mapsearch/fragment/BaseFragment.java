package cnpm.trung.mapsearch.fragment;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by sang on 6/7/2015.
 */
public class BaseFragment extends Fragment {

    protected AppCompatActivity activity;
    protected Context context;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (AppCompatActivity) activity;
        this.context = activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        activity = null;
    }
}
