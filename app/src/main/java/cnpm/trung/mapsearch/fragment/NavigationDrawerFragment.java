package cnpm.trung.mapsearch.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cnpm.trung.mapsearch.activity.MapSearchApplication;
import cnpm.trung.mapsearch.adapter.NavigationDrawerAdapter;
import cnpm.trung.mapsearch.configuration.APIConfig;
import cnpm.trung.mapsearch.event.DrawerItemClickEvent;
import cnpm.trung.mapsearch.logcat.CLog;
import cnpm.trung.mapsearch.models.Category;
import cnpm.trung.mapsearch.models.NavigationItem;
import cnpm.trung.mapsearch.R;
import cnpm.trung.mapsearch.activity.NavigationDrawerCallbacks;
import cnpm.trung.mapsearch.network.CUrl;
import de.greenrobot.event.EventBus;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawer.
 * See the <a href="https://developer.android.com/design/patterns/navigation-drawer.html#Interaction">
 * design guidelines</a> for a complete explanation of the behaviors implemented here.
 */
public class NavigationDrawerFragment extends BaseFragment {


    private static final String TAG = NavigationDrawerFragment.class.getSimpleName();

    private NavigationDrawerCallbacks mCallbacks;

    private ActionBarDrawerToggle mActionBarDrawerToggle;

    private DrawerLayout mDrawerLayout;
    private RecyclerView mDrawerList;
    private View mFragmentContainerView;
    private NavigationDrawerAdapter adapter;

    private int mCurrentSelectedPosition = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        mDrawerList = (RecyclerView) view.findViewById(R.id.drawerList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mDrawerList.setLayoutManager(layoutManager);
        mDrawerList.setHasFixedSize(true);

        final List<NavigationItem> navigationItems = getMenu();
        adapter = new NavigationDrawerAdapter(navigationItems);
//        adapter.setNavigationDrawerCallbacks(this);
        mDrawerList.setAdapter(adapter);

        initData();
        return view;
    }

    private void addCategoryAllAndFavorite() {
        Category categoryObject = new Category();
        categoryObject.setCat_id(cnpm.trung.mapsearch.configuration.Configuration.CATEGORY_ALL);
        categoryObject.setCat_name("All");
        adapter.add(categoryObject);

        categoryObject = new Category();
        categoryObject.setCat_id(cnpm.trung.mapsearch.configuration.Configuration.CATEGORY_FAVORITE);
        categoryObject.setCat_name("Favorite");
        adapter.add(categoryObject);


    }

    private void initData() {
        String url = APIConfig.getCategoryUrl();
        CLog.d(TAG, "get category url:" + url);
        final ProgressDialog mDialog = new ProgressDialog(context);
        mDialog.setMessage("Loading...");
        mDialog.show();
        addCategoryAllAndFavorite();
        CUrl.get(context, url, Category[].class, new CUrl.CUrlCallback<Category[]>() {
            @Override
            public void onResponse(Category[] result) {
                mDialog.dismiss();
                if (result != null && result.length > 0) {
                    adapter.allAll(result);
                } else {
                    Toast.makeText(context, getString(R.string.error_connect), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    public ActionBarDrawerToggle getActionBarDrawerToggle() {
        return mActionBarDrawerToggle;
    }

    public DrawerLayout getDrawerLayout() {
        return mDrawerLayout;
    }



    public List<NavigationItem> getMenu() {
        List<NavigationItem> items = new ArrayList<NavigationItem>();
        return items;
    }

    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     * @param toolbar      The Toolbar of the activity.
     */
    public void setup(int fragmentId, DrawerLayout drawerLayout, Toolbar toolbar) {
        mFragmentContainerView = (View) getActivity().findViewById(fragmentId).getParent();
        mDrawerLayout = drawerLayout;

        mDrawerLayout.setStatusBarBackgroundColor(getResources().getColor(R.color.myPrimaryDarkColor));

        mActionBarDrawerToggle = new ActionBarDrawerToggle(getActivity(), mDrawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) return;

                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!isAdded()) return;
                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }
        };

        // If the user hasn't 'learned' about the drawer, open it to introduce them to the drawer,
        // per the navigation drawer design guidelines.

        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mActionBarDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);
    }

    public void onEvent(DrawerItemClickEvent event) {

        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        if(event.position != -1) {
            MapSearchApplication.getInstance().currentPosition = event.position;
//            adapter.selectPosition(event.position);
        }
        adapter.notifyDataSetChanged();
    }

    private void selectItem(int position) {
        mCurrentSelectedPosition = position;
        MapSearchApplication.getInstance().currentPosition = position;
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected(position);
        }

        Category category = adapter.getItemAtPosition(position);
        ((NavigationDrawerAdapter) mDrawerList.getAdapter()).selectPosition(position);
    }

    public void openDrawer() {
        mDrawerLayout.openDrawer(mFragmentContainerView);
    }

    public void closeDrawer() {
        mDrawerLayout.closeDrawer(mFragmentContainerView);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }

        EventBus.getDefault().register(this);
    }



    @Override
    public void onDetach() {
        EventBus.getDefault().unregister(this);
        super.onDetach();
        mCallbacks = null;
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.
        mActionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

}
