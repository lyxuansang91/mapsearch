package cnpm.trung.mapsearch.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import cnpm.trung.mapsearch.R;
import cnpm.trung.mapsearch.activity.MapSearchApplication;
import cnpm.trung.mapsearch.activity.NavigationDrawerCallbacks;
import cnpm.trung.mapsearch.event.DrawerItemClickEvent;
import cnpm.trung.mapsearch.logcat.CLog;
import cnpm.trung.mapsearch.models.Category;
import de.greenrobot.event.EventBus;


public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.ViewHolder> {

    private static final String TAG = NavigationDrawerAdapter.class.getSimpleName();
    private List mData;
    private NavigationDrawerCallbacks mNavigationDrawerCallbacks;

    public NavigationDrawerAdapter(List data) {
        mData = data;
    }

    @Override
    public NavigationDrawerAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.drawer_row, viewGroup, false);
        final ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NavigationDrawerAdapter.ViewHolder viewHolder, int i) {

        Object object = mData.get(i);
        if (object instanceof Category) {
            viewHolder.textView.setText(((Category) object).getCat_name());
//            CLog.d(TAG, "position: " + i + ", current position:" + MapSearchApplication.getInstance().currentPosition);
            if (i == MapSearchApplication.getInstance().currentPosition) {
                viewHolder.itemView.setBackgroundColor(viewHolder.itemView.getResources().getColor(R.color.grayColor));
            } else
                viewHolder.itemView.setBackgroundColor(viewHolder.itemView.getResources().getColor(R.color.myDrawerBackground));
        }
    }


    public void selectPosition(int position) {
        MapSearchApplication.getInstance().currentPosition = position;
        notifyItemChanged(position);
    }

    @Override
    public int getItemCount() {
        return mData != null ? mData.size() : 0;
    }

    public void add(Category category) {
        mData.add(category);
        notifyItemInserted(mData.size() - 1);
    }

    public Category getItemAtPosition(int position) {
        return (Category) mData.get(position);
    }

    public void allAll(Category[] result) {
        for(Category category : result) {
            mData.add(category);
        }
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.item_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    Category category = (Category) mData.get(position);
                    EventBus.getDefault().post(new DrawerItemClickEvent(category, position));
                }
            });
        }
    }
}