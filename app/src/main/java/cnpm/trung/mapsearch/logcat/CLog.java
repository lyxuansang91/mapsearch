package cnpm.trung.mapsearch.logcat;

import android.util.Log;

import cnpm.trung.mapsearch.configuration.Configuration;


/**
 * Created by sang on 29/01/2015.
 */
public class CLog {

    public static int w(String tag, String msg) {
        if (Configuration.DEBUG)
            return Log.w(tag, msg);
        return -1;
    }

    public static int d(String tag, String msg) {
        if (Configuration.DEBUG)
            return Log.d(tag, msg);
        return -1;
    }

    public static int e(String tag, String msg) {
        if (Configuration.DEBUG)
            return Log.e(tag, msg);
        return -1;
    }

    public static int v(String tag, String msg) {
        if (Configuration.DEBUG)
            return Log.v(tag, msg);
        return -1;
    }

    public static int i(String tag, String msg) {
        if (Configuration.DEBUG)
            return Log.i(tag, msg);
        return -1;
    }
}
