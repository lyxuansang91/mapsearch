package cnpm.trung.mapsearch.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sang on 6/7/2015.
 */
public class POI implements Parcelable {
    private int id;
    private String cat_id;
    private String marker;
    public String marker_id;
    private double latitude;
    private double longitude;
    private String name;
    private String address;
    private String created_at;
    private String updated_at;
    private String description;

    @Override
    public String toString() {
        return "POI{" +
                "id=" + id +
                ", marker='" + marker + '\'' +
                ", marker_id='" + marker_id + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getMarker() {
        return marker;
    }

    public void setMarker(String marker) {
        this.marker = marker;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongtitude() {
        return longitude;
    }

    public void setLongtitude(double longtitude) {
        this.longitude = longtitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public POI() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(marker);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(description);
    }

    private static POI getNewFromParcel(Parcel source) {
        POI poi = new POI();
        poi.id = source.readInt();
        poi.name = source.readString();
        poi.address = source.readString();
        poi.marker = source.readString();
        poi.latitude = source.readDouble();
        poi.longitude = source.readDouble();
        poi.description = source.readString();
        return poi;
    }

    public static final Parcelable.Creator<POI> CREATOR = new Parcelable.Creator<POI>() {

        @Override
        public POI createFromParcel(Parcel source) {
            return POI.getNewFromParcel(source);
        }

        @Override
        public POI[] newArray(int size) {
            return new POI[size];
        }
    };
}
