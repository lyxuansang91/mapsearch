package cnpm.trung.mapsearch.network;


import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import cnpm.trung.mapsearch.logcat.CLog;


/**
 * Created by thuc on 12/1/14.
 */
public class CUrl {
    public static final String TAG = CUrl.class.getSimpleName();
    public static String apiKey = null;
    public static boolean usingSameHttpClientForAllRequest = true;

    private static HttpClient httpClient = null;

    public static <T> void get(final Context ctx, final String url, final Class<T> type, final CUrlCallback<T> callback) {
        HttpGet request = get(ctx, url);
        request(ctx, request, type, callback);
    }

    public static <T> void get(final Context ctx, final String url, ArrayList<NameValuePair> headerParams, final Class<T> type, final CUrlCallback<T> callback) {
        HttpGet request = get(ctx, url, headerParams);
        request(ctx, request, type, callback);
    }

    public static <T> void post(final Context ctx, final String url, ArrayList<NameValuePair> postParams, final Class<T> type, final CUrlCallback<T> callback) {
        HttpPost request = post(ctx, url, postParams);
        request(ctx, request, type, callback);
    }

    public static <T> void post(final Context ctx, final String url, String payload, final Class<T> type, final CUrlCallback<T> callback) {
        HttpPost request = post(ctx, url, payload);
        request(ctx, request, type, callback);
    }

    public static <T> void post(final Context ctx, final String url, final ArrayList<NameValuePair> headerParams, String payload, final Class<T> type, final CUrlCallback<T> callback) {
        HttpPost request = post(ctx, url, headerParams, payload);
        CLog.d(TAG, "curl payload:" + payload);
        request(ctx, request, type, callback);
    }

    public static HttpGet get(final Context ctx, final String url) {
        return (HttpGet) getRequest(ctx, url, false, null, null);
    }

    public static HttpGet get(final Context ctx, final String url, final ArrayList<NameValuePair> headerParams) {
        return (HttpGet) getRequest(ctx, url, headerParams, false, null, null);
    }

    public static HttpPost post(final Context ctx, final String url, ArrayList<NameValuePair> postParams) {
        return (HttpPost) getRequest(ctx, url, true, postParams, null);
    }

    public static HttpPost post(final Context ctx, final String url, String payload) {
        return (HttpPost) getRequest(ctx, url, true, null, payload);
    }

    public static HttpPost post(final Context ctx, final String url, String payload, ArrayList<NameValuePair> headerParams) {
        return (HttpPost) getRequest(ctx, url, true, null, payload);
    }

    public static HttpPost post(final Context ctx, final String url, final ArrayList<NameValuePair> headerParams, String payload) {
        return (HttpPost) getRequest(ctx, url, headerParams, true, null, payload);
    }

    public static HttpRequestBase getRequest(final Context ctx, final String url, final ArrayList<NameValuePair> headerParams, final boolean isPost, final ArrayList<NameValuePair> postParams, final String payload) {
        final HttpRequestBase request;

        try {
            if (!isPost) {
                request = new HttpGet(url);
            } else {
                request = new HttpPost(url);
            }

            if (apiKey != null) {
                request.addHeader("Accept", "application/json");
//            request.addHeader("Content-Type", "application/json");
            }

            if (headerParams != null)
                for (NameValuePair nameValuePair : headerParams)
                    request.addHeader(nameValuePair.getName(), nameValuePair.getValue());

            if (isPost) {
                if (postParams != null) {
                    CLog.d(TAG, "POST request with form data");
                    UrlEncodedFormEntity entity = null;
                    try {
                        entity = new UrlEncodedFormEntity(postParams);
                        ((HttpPost) request).setEntity(entity);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        Log.e(TAG, e.getMessage());
                    }
                } else if (payload != null) {
                    request.addHeader("Content-Type", "application/json");
                    CLog.d(TAG, "POST request with raw data payload");
                    try {
                        ((HttpPost) request).setEntity(new StringEntity(payload));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        Log.e(TAG, e.getMessage());
                    }
                } else {
                    CLog.d(TAG, "POST request without payload");
                }
            } else {
                CLog.d(TAG, "GET request");
            }

            return request;

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static HttpRequestBase getRequest(final Context ctx, final String url, final boolean isPost, final ArrayList<NameValuePair> postParams, final String payload) {
        return getRequest(ctx, url, null, isPost, postParams, payload);
//        final HttpRequestBase request;
//
//        if (!isPost) {
//            request = new HttpGet(url);
//        }
//        else {
//            request = new HttpPost(url);
//        }
//
//        if (apiKey != null) {
//            request.addHeader("Accept", "application/json");
//            request.addHeader("Content-Type", "application/json");
//        }
//
//        if (isPost) {
//            if (postParams != null) {
//                CLog.d(TAG, "POST request with form data");
//                UrlEncodedFormEntity entity = null;
//                try {
//                    entity = new UrlEncodedFormEntity(postParams);
//                    ((HttpPost) request).setEntity(entity);
//                } catch (UnsupportedEncodingException e) {
//                    e.printStackTrace();
//                    Log.e(TAG, e.getMessage());
//                }
//            }
//            else if (payload != null) {
//                CLog.d(TAG, "POST request with raw data payload");
//                try {
//                    ((HttpPost) request).setEntity(new StringEntity(payload));
//                } catch (UnsupportedEncodingException e) {
//                    e.printStackTrace();
//                    Log.e(TAG, e.getMessage());
//                }
//            }
//            else {
//                CLog.d(TAG, "POST request without payload");
//            }
//        }
//        else {
//            CLog.d(TAG, "GET request");
//        }
//
//        return request;
    }

    public static <T> void request(final Context ctx, final HttpRequestBase request, final Class<T> type, final CUrlCallback<T> callback) {
        new AsyncTask<Void, Void, T>() {

            @Override
            protected T doInBackground(Void... params) {
                try {
                    CLog.d(TAG, "Requesting: " + request.getURI());

                    HttpClient httpClient = getHttpClient(usingSameHttpClientForAllRequest);

                    HttpResponse response = httpClient.execute(request);
                    String responseString = EntityUtils.toString(response.getEntity());

                    CLog.d(TAG, "Raw response: " + responseString);

                    if (type.isAssignableFrom(String.class)) {
                        return (T) responseString;
                    } else {
                        final T data = JsonHelper.decode(responseString, type);
                        return data;
                    }


                } catch (IOException e) {
                    e.printStackTrace();

                    return null;
                }

            }

            @Override
            protected void onPostExecute(T response) {
                callback.onResponse(response);
            }
        }.execute();
    }

    public static HttpClient getHttpClient(boolean createNew) {
        if (!createNew && httpClient != null) {
            CLog.d(TAG, "Using existing httpClient");
            return httpClient;
        }

        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            HttpConnectionParams.setConnectionTimeout(params, 3000);
            HttpConnectionParams.setSoTimeout(params, 5000);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

            httpClient = new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            httpClient = new DefaultHttpClient();
        }

        return httpClient;
    }

    public interface CUrlCallback<T> {
//        void onCacheResponse(T result);

        void onResponse(T result);
    }

    public static class MySSLSocketFactory extends SSLSocketFactory {
        SSLContext sslContext = SSLContext.getInstance("TLS");

        public MySSLSocketFactory(KeyStore truststore)
                throws NoSuchAlgorithmException, KeyManagementException,
                KeyStoreException, UnrecoverableKeyException {
            super(truststore);

            TrustManager tm = new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };

            sslContext.init(null, new TrustManager[]{tm}, null);
        }

        @Override
        public Socket createSocket(Socket socket, String host, int port,
                                   boolean autoClose) throws IOException, UnknownHostException {
            return sslContext.getSocketFactory().createSocket(socket, host, port,
                    autoClose);
        }

        @Override
        public Socket createSocket() throws IOException {
            return sslContext.getSocketFactory().createSocket();
        }
    }
}