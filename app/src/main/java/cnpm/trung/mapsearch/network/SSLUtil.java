package cnpm.trung.mapsearch.network;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

public class SSLUtil {
    public SSLContext getSSLContext() throws NoSuchAlgorithmException,
            KeyManagementException {
        SSLContext localSSLContext = SSLContext.getInstance("TLS");
        TrustManager[] arrayOfTrustManager = new TrustManager[1];
        arrayOfTrustManager[0] = new TrustAllTrustManager();
        localSSLContext.init(null, arrayOfTrustManager, new SecureRandom());
        return localSSLContext;
    }
}
