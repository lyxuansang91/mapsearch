package cnpm.trung.mapsearch.network;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import cnpm.trung.mapsearch.logcat.CLog;


/**
 * Created by thuc on 12/1/14.
 */
public class UrlHelper {
    public static final String TAG = UrlHelper.class.getSimpleName();

    public static <T> void get(final Context ctx, final String url, final Class<T> type, final ArrayList<NameValuePair> listHeader, final CUrlCallback<T> callback) {
        request(ctx, url, type, callback, false, null, listHeader, null);
    }

    public static <T> void post(final Context ctx, final String url, ArrayList<NameValuePair> postParams, final ArrayList<NameValuePair> listHeader, final Class<T> type, final CUrlCallback<T> callback) {
        request(ctx, url, type, callback, true, postParams, listHeader, null);
    }

    public static <T> void post(final Context ctx, final String url, String payload, final Class<T> type, final CUrlCallback<T> callback) {
        request(ctx, url, type, callback, true, null, null, payload);
    }

    public static <T> void request(final Context ctx, final String url, final Class<T> type,
                                   final CUrlCallback<T> callback, final boolean isPost,
                                   final ArrayList<NameValuePair> postParams,
                                   final ArrayList<NameValuePair> listHeader,
                                   final String payload) {

        new AsyncTask<String, Void, T>() {

            @Override
            protected T doInBackground(String... params) {
                String url = params[0];

                Log.d(TAG, "Getting: " + url);

                HttpRequestBase request;
                if (!isPost) {
                    request = new HttpGet(url);
                } else {
                    request = new HttpPost(url);
                }
//        httpGet.addHeader(HEADER_APPWHOOSH_PACKAGE_NAME, ctx.getPackageName());
//        httpGet.addHeader(HEADER_APPWHOOSH_FINGERPRINT, AccountGeneral.getSHA1Fingerprint(ctx));
                request.addHeader("Accept", "application/json");
                if (listHeader != null) {
                    for (NameValuePair keyValueObject : listHeader)
                        request.addHeader(keyValueObject.getName().toString(), keyValueObject.getValue().toString());
                }


                try {
                    HttpClient httpClient = getNewHttpClient();

                    if (isPost) {
                        if (postParams != null) {
                            UrlEncodedFormEntity entity = null;
                            try {
                                entity = new UrlEncodedFormEntity(postParams);
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                                Log.e(TAG, e.getMessage());
                                return null;
                            }
                            ((HttpPost) request).setEntity(entity);
                        } else if (payload != null) {
                            try {
                                ((HttpPost) request).setEntity(new StringEntity(payload));
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                                Log.e(TAG, e.getMessage());
                                return null;
                            }
                        }
                    }

                    HttpResponse response = httpClient.execute(request);
                    String responseString = EntityUtils.toString(response.getEntity());

                    CLog.d(TAG, "Raw response: " + responseString);

                    if (type.isAssignableFrom(String.class)) {
                        return (T) responseString;
                    } else {
                        final T data = JsonHelper.decode(responseString, type);
                        return data;
                    }


                } catch (IOException e) {
                    e.printStackTrace();

                    return null;
                }

            }

            @Override
            protected void onPostExecute(T response) {
                callback.onResponse(response);
            }
        }.execute(url);


    }

    public static HttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            HttpConnectionParams.setConnectionTimeout(params, 3000);
            HttpConnectionParams.setSoTimeout(params, 5000);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }

    public interface CUrlCallback<T> {
        void onResponse(T result);
    }

    public static class MySSLSocketFactory extends SSLSocketFactory {
        SSLContext sslContext = SSLContext.getInstance("TLS");

        public MySSLSocketFactory(KeyStore truststore)
                throws NoSuchAlgorithmException, KeyManagementException,
                KeyStoreException, UnrecoverableKeyException {
            super(truststore);

            TrustManager tm = new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain,
                                               String authType) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };

            sslContext.init(null, new TrustManager[]{tm}, null);
        }

        @Override
        public Socket createSocket(Socket socket, String host, int port,
                                   boolean autoClose) throws IOException, UnknownHostException {
            return sslContext.getSocketFactory().createSocket(socket, host, port,
                    autoClose);
        }

        @Override
        public Socket createSocket() throws IOException {
            return sslContext.getSocketFactory().createSocket();
        }
    }
}
