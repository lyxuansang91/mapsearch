package cnpm.trung.mapsearch.event;

import cnpm.trung.mapsearch.models.Category;

/**
 * Created by sang on 6/7/2015.
 */
public class DrawerItemClickEvent {
    public Category category;
    public int position;
    public DrawerItemClickEvent(Category category, int position) {
        this.category = category;
        this.position = position;
    }
}
