package cnpm.trung.mapsearch.activity;

import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import cnpm.trung.mapsearch.R;

/**
 * Created by sang on 6/7/2015.
 */
public class BaseActivity extends AppCompatActivity {

    public void handleNoNetwork() {
        Toast.makeText(getApplicationContext(), getString(R.string.error_connect), Toast.LENGTH_SHORT).show();
    }
}
