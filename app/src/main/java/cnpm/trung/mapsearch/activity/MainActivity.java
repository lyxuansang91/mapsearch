package cnpm.trung.mapsearch.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cnpm.trung.mapsearch.R;
import cnpm.trung.mapsearch.configuration.APIConfig;
import cnpm.trung.mapsearch.configuration.Configuration;
import cnpm.trung.mapsearch.databases.MarkerDatabaseHandler;
import cnpm.trung.mapsearch.event.DrawerItemClickEvent;
import cnpm.trung.mapsearch.fragment.NavigationDrawerFragment;
import cnpm.trung.mapsearch.logcat.CLog;
import cnpm.trung.mapsearch.models.Category;
import cnpm.trung.mapsearch.models.POI;
import cnpm.trung.mapsearch.network.CUrl;
import de.greenrobot.event.EventBus;


public class MainActivity extends BaseActivity
        implements NavigationDrawerCallbacks, OnMapReadyCallback {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String KEY_POI = "poi";
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private Toolbar mToolbar;
    private GoogleMap mMap;

    private Handler handler = new Handler();
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);
//        mToolbar.setVisibility(View.GONE);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer), mToolbar);

        setUpMapIfNeeded();


    }

    private void initData() {
        Uri data = getIntent().getData();
        if(data == null) {
            Category category = new Category();
            category.setCat_name("All");
            category.setCat_id(Configuration.CATEGORY_ALL);
//        addMyLocation();
            addMarkerByCategory(category);
        } else {
            List<String> params = data.getPathSegments();
            int segmentSize = params.size();
            String id = null;
            if(segmentSize > 0) {
                id = params.get(segmentSize - 1);
            }

            getMarkerFromId(id);



        }
//        addFavoriteMarker();
    }

    private void getMarkerFromId(String id) {
        String url = APIConfig.getDetailUrl(Integer.parseInt(id));
        CUrl.get(MainActivity.this, url, POI[].class, new CUrl.CUrlCallback<POI[]>() {
            @Override
            public void onResponse(POI[] result) {
                if(result != null && result.length > 0) {
                    addPOI(result[0], true);
                }
            }
        });
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
    }

    private void addMyLocation() {
        Location myLocation = mMap.getMyLocation();
        if (myLocation != null) {
            LatLng loc = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
            mMap.addMarker(new MarkerOptions().position(loc).title(getString(R.string.mylocation)));
            if (mMap != null) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 16.0f));
            }

        }
    }

    private void addFavoriteMarker() {
        new AsyncTask<Void, Void, ArrayList<POI>>() {

            @Override
            protected void onPostExecute(ArrayList<POI> pois) {
                dialog.dismiss();
                if(pois != null && pois.size() > 0){
                    mMap.clear();
                    addMyLocation();
                    for(POI poi : pois) {
                        addPOI(poi, false);
                    }
                }
            }

            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(MainActivity.this, "", "Loading..");
            }

            @Override
            protected ArrayList<POI> doInBackground(Void... params) {
                MarkerDatabaseHandler databaseHandler = new MarkerDatabaseHandler(MainActivity.this);
                ArrayList<POI> listFavorites = databaseHandler.getAllMarkers();
                databaseHandler.close();
                return listFavorites;
            }
        }.execute();


    }


    public void onEvent(DrawerItemClickEvent event) {
        CLog.d(TAG, "(on drawer item click event)");
        if(event.category.getCat_id() == Configuration.CATEGORY_FAVORITE) {
            addFavoriteMarker();
        } else addMarkerByCategory(event.category);

    }

    private void addPOI(POI poi, final boolean isFavorite) {
        final POI newPOI = poi;
        CLog.d(TAG, "new poi:" + newPOI.toString());
        final String link = APIConfig.getMarkerUrl(newPOI.getMarker());
//        CLog.d(TAG, "link marker:" + link);
        final MarkerOptions markerOption = new MarkerOptions()
                .position(new LatLng(poi.getLatitude(), poi.getLongtitude()))
                .draggable(false)
                .visible(true)
                .title(poi.getName())
                .snippet(poi.getAddress());

        if(!isFavorite) {
            new AsyncTask<Void, Void, Bitmap>() {

                @Override
                protected Bitmap doInBackground(Void... params) {
                    try {
                        return Picasso.with(MainActivity.this).load(link).get();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                        return null;
                    }
                }

                @Override
                protected void onPostExecute(Bitmap bitmap) {
                    super.onPostExecute(bitmap);
                    if (bitmap != null) {
                            markerOption.icon(BitmapDescriptorFactory.fromBitmap(bitmap));
//                    CLog.d(TAG, "bitmap is not null");
                    }
                    if (mMap != null) {
                        Marker marker = mMap.addMarker(markerOption);
//                    marker.showInfoWindow();
                        newPOI.marker_id = marker.getId();
                        CLog.d(TAG, "add marker:" + marker.getId());
                        boolean found = false;
                        for (POI poi : MapSearchApplication.getInstance().poisList) {
                            if (poi.marker_id.equals(marker.getId())) {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                            MapSearchApplication.getInstance().poisList.add(newPOI);
                    }
                }
            }.execute();
        } else {
           mMap.addMarker(markerOption);
           mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(markerOption.getPosition(), 16.0f));
           mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(markerOption.getPosition(), 16.0f));
        }

    }


    private void addNewPOIList(POI[] pois) {
        for (POI poi : pois) {
            addPOI(poi, false);
        }
    }

    private long back_pressed;

    private void addMarkerByCategory(final Category category) {
        String url = (category.getCat_id() != 0 ? APIConfig.getPOIByCategoryUrl(category.getCat_id()): APIConfig.getAllPOIUrl());
        CLog.d(TAG, "get category url:" + url);
        dialog = ProgressDialog.show(MainActivity.this, "", "Loading..");
        CUrl.get(MainActivity.this, url, POI[].class, new CUrl.CUrlCallback<POI[]>() {
            @Override
            public void onResponse(POI[] result) {
                dialog.dismiss();
                if (result != null &&  result.length > 0) {
                    mMap.clear();
                    addMyLocation();
                    addNewPOIList(result);
                } else {
                    handleNoNetwork();
                }
            }
        });
    }

    private void addMarker(LatLng latLng) {
        MarkerOptions markerOption = new MarkerOptions()
                .position(latLng)
                .draggable(false)
                .visible(true);

        mMap.addMarker(markerOption);

    }


    private void setUpMap() {
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.getUiSettings().setScrollGesturesEnabled(true);
        mMap.getUiSettings().setTiltGesturesEnabled(true);
        initData();
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                CLog.d(TAG, "marker id:" + marker.getId());
                POI thisPOI = null;
                for(POI poi : MapSearchApplication.getInstance().poisList) {
                    if(poi.marker_id.equals(marker.getId())) {
                        thisPOI = poi;
                        break;
                    }

                }

                if(thisPOI != null) {
                    //found
//                    Toast.makeText(MainActivity.this, thisPOI.getAddress(), Toast.LENGTH_SHORT).show();
                    Bundle bundle = new Bundle();
                    bundle.putParcelable(KEY_POI, thisPOI);
                    Intent intent = new Intent(getApplicationContext(), DetailPOI.class);
                    intent.putExtras(bundle);
                    startActivity(intent);

                }

            }
        });
    }


    private void addFragment(Fragment fragment, int resID, String tag) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(resID, fragment, tag);
        ft.commit();
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
//        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            SupportMapFragment supportMapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
            supportMapFragment.getMapAsync(this);
        }

    }


    @Override
    public void onBackPressed() {
        if (mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.closeDrawer();
        else {
//            super.onBackPressed();
            if(System.currentTimeMillis() - back_pressed < 2000) {
                finish();
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.backAgainToExit), Toast.LENGTH_SHORT).show();
            }
            back_pressed = System.currentTimeMillis();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        setUpMap();
    }
}
