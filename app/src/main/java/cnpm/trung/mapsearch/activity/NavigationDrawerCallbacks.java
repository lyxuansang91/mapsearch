package cnpm.trung.mapsearch.activity;

public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
