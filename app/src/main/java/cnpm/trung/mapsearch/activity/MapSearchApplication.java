package cnpm.trung.mapsearch.activity;

import android.app.Application;

import java.util.ArrayList;

import cnpm.trung.mapsearch.models.POI;

/**
 * Created by sang on 6/7/2015.
 */
public class MapSearchApplication extends Application {


    private static MapSearchApplication _instance;

    public int currentPosition;

    public static MapSearchApplication getInstance() {
        return _instance;
    }

    public ArrayList<POI> poisList;


    @Override
    public void onCreate() {
        super.onCreate();
        _instance = this;
        currentPosition = 0;
        poisList = new ArrayList<POI>();
    }
}
