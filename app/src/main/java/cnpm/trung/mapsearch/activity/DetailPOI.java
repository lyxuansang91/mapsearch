package cnpm.trung.mapsearch.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import cnpm.trung.mapsearch.R;
import cnpm.trung.mapsearch.configuration.APIConfig;
import cnpm.trung.mapsearch.databases.MarkerDatabaseHandler;
import cnpm.trung.mapsearch.logcat.CLog;
import cnpm.trung.mapsearch.models.POI;
import cnpm.trung.mapsearch.network.CUrl;

public class DetailPOI extends BaseActivity {


    private static final String KEY_POI = "poi";
    private static final String TAG = DetailPOI.class.getSimpleName();

    private TextView tvTitle;
    private TextView tvAddress;
    private TextView tvLatitude, tvLongtitude;
    private TextView tvDescription;
    private Button btnDirection;
    private Button btnFavorite;
    private boolean isFavorite;
    private POI currentPOI = null;

    private void initData() {
        if(currentPOI != null) {
            setContent(currentPOI);
            getDataDetail();
        }
    }

    private void setContent(POI result) {
        tvTitle.setText(result.getName());
        tvAddress.setText(result.getAddress());
        tvLatitude.setText(result.getLatitude() + "");
        tvLongtitude.setText(result.getLongtitude() + "");
        getSupportActionBar().setTitle(result.getName() + "-" + result.getAddress());
        String description = result.getDescription();

        if(description != null && !description.isEmpty()) {
            try {
                tvDescription.setText(Html.fromHtml(description));
            } catch(Exception ex) {
                tvDescription.setText(getString(R.string.no_info));
            }
        } else
            tvDescription.setText(getString(R.string.no_info));
    }


    private void getDataDetail() {
        if(currentPOI != null) {
            String url = APIConfig.getDetailUrl(currentPOI.getId());
            CUrl.get(DetailPOI.this, url, POI[].class, new CUrl.CUrlCallback<POI[]>() {
                @Override
                public void onResponse(POI[] result) {
                    if(result != null && result.length > 0) {
                        setContent(result[0]);
                    }
                }
            });
        }
    }

    private void initUI() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvAddress = (TextView) findViewById(R.id.tvAddress);
        tvLatitude = (TextView) findViewById(R.id.tvLatitude);
        tvLongtitude = (TextView) findViewById(R.id.tvLongtitude);
        tvDescription = (TextView) findViewById(R.id.tvDescription);
        btnDirection = (Button) findViewById(R.id.btnDirection);
        btnFavorite = (Button) findViewById(R.id.btn_favorite);
        btnFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isFavorite) {
                    removeFavorite();
                } else addFavorite();
                checkFavorite();
            }
        });

        btnDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(DetailPOI.this, getString(R.string.update_option), Toast.LENGTH_SHORT).show();
            }
        });

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_poi);
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            currentPOI = bundle.getParcelable(KEY_POI);
            CLog.d(TAG, "current poi marker:" + currentPOI.getMarker());
//            Toast.makeText(getApplicationContext(), currentPOI.getAddress(), Toast.LENGTH_SHORT).show();

        }
        initUI();
        initData();
        checkFavorite();
    }

    private void addFavorite() {
        MarkerDatabaseHandler databaseHandler = new MarkerDatabaseHandler(DetailPOI.this);
        boolean isAdded = databaseHandler.addNewMarker(currentPOI);
        Toast.makeText(DetailPOI.this, isAdded ? "Đã thích thành công" : "Không thể thực hiện chức năng này", Toast.LENGTH_SHORT).show();
        databaseHandler.close();
    }

    private void removeFavorite() {
        MarkerDatabaseHandler databaseHandler = new MarkerDatabaseHandler(DetailPOI.this);
        boolean isRemoved = databaseHandler.removePOI(currentPOI.getId());
        Toast.makeText(DetailPOI.this, isRemoved ? "Bỏ thích thành công" : "Không thể thực hiện chức năng này", Toast.LENGTH_SHORT).show();
        databaseHandler.close();
    }

    private void checkFavorite() {
        MarkerDatabaseHandler databaseHandler = new MarkerDatabaseHandler(DetailPOI.this);
        POI poi = databaseHandler.getPOIByID(currentPOI.getId());
        isFavorite = (poi != null);
        btnFavorite.setTextColor(isFavorite ? getResources().getColor(R.color.white) : getResources().getColor(R.color.orange));
        btnFavorite.setBackgroundResource(isFavorite ? R.drawable.xml_bg_button_unfavorite : R.drawable.xml_bg_button_favorite);
        btnFavorite.setText(isFavorite ? getResources().getString(R.string.unfavorite) : getResources().getString(R.string.favorite));
        databaseHandler.close();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detail_poi, menu);
        return true;
    }

    private void shareLocation() {
        Intent shareintent = new Intent();
        shareintent.setAction(Intent.ACTION_SEND);
        shareintent.putExtra(Intent.EXTRA_TEXT, APIConfig.getDetailPOIUrl(currentPOI.getId()));
        shareintent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.default_share_content));
        shareintent.setType("text/plain");
        startActivity(Intent.createChooser(shareintent, "Share via ..."));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if(id == R.id.share) {
            shareLocation();
            return true;
        }

        if(id == android.R.id.home) {
            super.onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
