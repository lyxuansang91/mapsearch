package cnpm.trung.mapsearch.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import cnpm.trung.mapsearch.models.POI;


/**
 * Created by sang on 15/12/2014.
 */
public class MarkerDatabaseHandler extends SQLiteOpenHelper {

    //Database version
    private static final int DATABASE_VERSION = 3;

    //Database name
    private static final String DATABASE_NAME = "Marker";

    //Songs table name
    private static final String TABLE_MARKER = "Marker";

    //Song table columns names
    private static final String KEY_ID = "_marker_id";
    private static final String KEY_NAME = "_marker_name";
    private static final String KEY_ADDRESS = "_marker_address";
    private static final String KEY_MARKER = "_marker";
    private static final String KEY_LATITUDE = "_latitude";
    private static final String KEY_LONGTITUDE = "_longtitude";
    private static final String KEY_DESCRIPTION = "_description";

    public MarkerDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_MARKER_TABLE =
                "CREATE TABLE " + TABLE_MARKER + "("
                        + KEY_ID + " INTEGER PRIMARY KEY, "
                        + KEY_NAME + " TEXT, "
                        + KEY_ADDRESS + " TEXT, "
                        + KEY_MARKER + " TEXT, "
                        + KEY_LATITUDE + " REAL, "
                        + KEY_LONGTITUDE + " REAL, "
                        + KEY_DESCRIPTION + "TEXT)";
        sqLiteDatabase.execSQL(CREATE_MARKER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MARKER);
        onCreate(db);
    }


    public boolean addNewMarker(POI poi) {
        if (poi == null)
            return false;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, poi.getId());
        values.put(KEY_NAME, poi.getName());
        values.put(KEY_ADDRESS, poi.getAddress());
        values.put(KEY_MARKER, poi.getMarker());
        values.put(KEY_LATITUDE, poi.getLatitude());
        values.put(KEY_LONGTITUDE, poi.getLongtitude());
//        values.put(KEY_DESCRIPTION, poi.getDescription());
        long row_id = db.insert(TABLE_MARKER, null, values);
        db.close();
        return (row_id != -1);
    }


    public POI getPOIByID(int marker_id) {
        POI poi = null;
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.query(TABLE_MARKER, new String[]{KEY_ID, KEY_NAME, KEY_ADDRESS, KEY_MARKER, KEY_LATITUDE, KEY_LONGTITUDE}, KEY_ID + " = ?",
                    new String[]{String.valueOf(marker_id)}, null, null, null, null);

            boolean isExisted = false;
            if (cursor != null && cursor.moveToFirst()) {
                poi = new POI();
                poi.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                poi.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
                poi.setAddress(cursor.getString(cursor.getColumnIndex(KEY_ADDRESS)));
                poi.setMarker(cursor.getString(cursor.getColumnIndex(KEY_MARKER)));
                poi.setLatitude(cursor.getDouble(cursor.getColumnIndex(KEY_LATITUDE)));
                poi.setLongtitude(cursor.getDouble(cursor.getColumnIndex(KEY_LONGTITUDE)));
//                poi.setDescription(cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION)));
            }
            cursor.close();
            db.close();
            return poi;

        } catch(Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }


    public ArrayList<POI> getAllMarkers() {
        ArrayList<POI> poisList = null;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_MARKER, new String[]{KEY_ID, KEY_NAME, KEY_ADDRESS, KEY_MARKER, KEY_LATITUDE, KEY_LONGTITUDE}, null, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            poisList = new ArrayList<POI>();
            do {
                POI poi = new POI();
                poi.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
                poi.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
                poi.setAddress(cursor.getString(cursor.getColumnIndex(KEY_ADDRESS)));
                poi.setMarker(cursor.getString(cursor.getColumnIndex(KEY_MARKER)));
                poi.setLatitude(cursor.getDouble(cursor.getColumnIndex(KEY_LATITUDE)));
                poi.setLongtitude(cursor.getDouble(cursor.getColumnIndex(KEY_LONGTITUDE)));
//                poi.setDescription(cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION)));
                poisList.add(poi);
            } while (cursor.moveToNext());
            cursor.close();
        }
//        cursor.close();
        db.close();
        return poisList;
    }

    public boolean removePOI(int marker_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        int num_row_delete = db.delete(TABLE_MARKER, KEY_ID + " = ?", new String[]{String.valueOf(marker_id)});
        db.close();
        return (num_row_delete > 0);
    }


}
