package cnpm.trung.mapsearch.configuration;

/**
 * Created by sang on 29/01/2015.
 */
public class APIConfig {

    public static final String API_KEY = "AIzaSyBGYjXs9XO7TBsA2fi8E8W88SFVggah5Fs";


    public static final String ANDROID_API_KEY = "AIzaSyCQz3BSnVr0_RBUYEn5ai95ORcZyAprvY8";

    // api
    public static final String PLAYLIST_INFO = "https://www.googleapis.com/youtube/v3/playlists?key=%s&id=%s&part=id,contentDetails,snippet";
    public static final String PLAYLIST_VIDEOS_LIST = "https://www.googleapis.com/youtube/v3/playlistItems?key=%s&playlistId=%s&part=id,snippet,contentDetails&pageToken=%s";
    public static final String CHANNEL_VIDEOS_LIST = "https://www.googleapis.com/youtube/v3/search?key=%s&chanelId=%s&maxResults=20&part=snippet,id&order=date";
    private static final String API_ROOT = "http://look.treckking.com/";
    private static final String API_PUBLIC_PATH = "public/";

    private static String getBuildPath() {
        return API_ROOT + API_PUBLIC_PATH;
    }
    public static String getCategoryUrl() {
        return getBuildPath() + "mcatlist";
    }

    public static String getPOIByCategoryUrl(int categoryId) {
        return getBuildPath() + "mcatdetail/" + categoryId;
    }

    public static String getAllPOIUrl() {
        return getBuildPath() + "mlist_loc";
    }

    public static String getPOIDescriptionUrl(int poiID) {
        return getBuildPath() + "mloc_detail/" + poiID;
    }

    public static String getMarkerUrl(String marker) {
        return getBuildPath() + "img/markers/" + marker + ".png";
    }

    public static String getDetailPOIUrl(int id) {
        return getBuildPath() + "showloc_detail/" + id;
    }

    public static String getDetailUrl(int id) {
        return getBuildPath() + "mlocdetail/" + id;
    }

}
